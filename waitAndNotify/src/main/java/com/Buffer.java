package com;

import java.util.PriorityQueue;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Buffer {
    private static final Logger logger = LoggerFactory.getLogger(Buffer.class);
    private Queue<String> queue = new PriorityQueue<String>();

    public synchronized void put(String line){
        queue.add(line);
        logger.info("Thread [{}] added [{}]", Thread.currentThread().getName(), line);
        notifyAll();
    }

    public synchronized String pull(){
        while(queue.isEmpty()){
            try {
                logger.info("Thread [{}] will wait", Thread.currentThread().getName());
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        String line = queue.remove();
        logger.info("Thread [{}] removed [{}] from queue.", Thread.currentThread().getName(), line);
        return line;
    }
}
