package com;

import javax.swing.*;

/**
 * Created by ezdecsa on 13/04/2016.
 */
public class Producer implements Runnable{

    Buffer buffer;

    public Producer(Buffer buffer){
        this.buffer = buffer;
    }


    public void run() {
        String output = null;
        while(!"end".equals(output)) {
            output = JOptionPane.showInputDialog("add something to buffer");
            buffer.put(output);
        }
    }
}
