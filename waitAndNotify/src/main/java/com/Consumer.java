package com;

public class Consumer implements Runnable {

    Buffer buffer ;

    public Consumer(Buffer buffer){
        this.buffer = buffer;
    }

    public void run() {
        String line = null;
        do {
            line = buffer.pull();
        }while(!"end".equals(line));
        // will not exit from all consumers if multiple consumers are started.
    }
}
