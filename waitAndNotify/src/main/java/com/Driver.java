package com;

/**
 * Created by ezdecsa on 13/04/2016.
 */
public class Driver {

    public static void main(String[] args){
        new Driver();
    }

    public Driver(){
        Buffer buffer = new Buffer();
        Thread producer = new Thread(new Producer(buffer), "com.Producer1");
        Thread consumer1 = new Thread(new Consumer(buffer), "com.Consumer1");
        Thread consumer2 = new Thread(new Consumer(buffer), "com.Consumer2");
        Thread consumer3 = new Thread(new Consumer(buffer), "com.Consumer3");
        Thread consumer4 = new Thread(new Consumer(buffer), "com.Consumer4");
        Thread consumer5 = new Thread(new Consumer(buffer), "com.Consumer5");

        consumer1.start();
        consumer2.start();
        consumer3.start();
        consumer4.start();
        consumer5.start();
        producer.start();
    }

}
