public class TaskExtended extends Thread {
    @Override
    public void run(){
        //stuff goes here
        System.out.println("TaskExtended started");
        int count = 10;
        for (int i=0; i< count; i++){
            try {
                sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("TaskExtended ended");
    }
}
