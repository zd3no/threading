import javax.swing.*;

public class Task  implements Runnable{

    public void run() {
        //stuff goes here
        System.out.println("Task started");
        String result = JOptionPane.showInputDialog(this, "Enter Start, End and delay in seconds:");
        int start = Integer.parseInt(result.split(" ")[0]);
        int end = Integer.parseInt(result.split(" ")[1]);
        int delay = Integer.parseInt(result.split(" ")[2]);
        long delayInMilis = delay*1000;
        while(start <= end){
            System.out.println(start);
            start ++;
            try {
                Thread.currentThread().sleep(delayInMilis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Task ended");
    }

}
