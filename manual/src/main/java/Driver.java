public class Driver {

    public Driver() {
        System.out.println("main started");
        Thread t = new Thread(new Task());
        Thread t2 = new TaskExtended();
        t.start();
        System.out.println("main ended");
    }

    public static void main(String[] args) {
        new Driver();
    }
}
